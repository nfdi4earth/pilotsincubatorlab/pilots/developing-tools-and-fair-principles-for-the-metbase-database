# Developing Tools and FAIR Principles for the MetBase Database

## Introduction
**MetBase** is a long-established meteorite database that houses chemical, isotopic, and physical properties of meteorites. Founded over 25 years ago, it is widely used in the cosmochemistry community. However, it currently does not adhere to **FAIR** principles, making it difficult to interoperate with other databases such as **Astromat**, **EarthChem**, and **GEOROC**.

The goal of the project is to align and merge **MetBase** with **Astromat**, ensuring that it becomes interoperable with major geochemical databases. The development includes a new technical foundation for the web application, extending it to access multiple databases via a single GUI with additional functionalities.

## Objectives

1. **Align and Merge MetBase with Astromat**: To ensure full interoperability with **Astromat**, which also makes it interoperable with **EarthChem** and **GEOROC**.
   
2. **Create the Data-Access Visualizer**: Develop a new web application allowing simultaneous access and visualization of multiple datasets from various databases.

3. **Develop EPMA Data Tools**: Create tools for making raw instrument data **FAIR**, using the **Kadi4Mat** ELN and web-app to process the data before user access.

4. **Foster Community Adoption**: Encourage the community to adopt **FAIR** practices and engage in software development through events like hackathons.

## Key Features

- **MetBase and Astromat Merger**: MetBase has been integrated into **Astromat**, which extended its schema and vocabularies to accommodate new metadata. This results in a single, sustainable database supported by NASA.
  
- **Data-Access Visualizer**: A new platform to access and visualize data from multiple geo- and cosmochemical databases. The app allows users to select, package, and manipulate datasets through an easily extendable framework.

- **EPMA Data Tools**: These tools leverage the **Kadi4Mat** ELN to process raw EPMA data into standardized, **FAIR** formats before making them accessible to researchers. Data can be filtered, sorted, and formatted for use in publications.

## Outcomes

- **Interoperability**: **MetBase** is now aligned with major geochemical databases, making it fully interoperable and **FAIR**-compliant.
  
- **Technical Advancements**: The development of the **Data-Access Visualizer** has provided a flexible, modular tool that can be expanded with new functionalities and tools.

- **Sustainability**: The successful integration of **MetBase** into **Astromat** ensures its long-term sustainability, supported by **NASA** and linked with other global databases.

## Challenges and Gaps

Aligning and merging **MetBase** with **Astromat** and other databases presented significant challenges due to legacy data structures. Mapping vocabularies and metadata between databases is complex, especially for older systems that didn’t originally follow standardized practices. The development of new workflows for data entry and processing (e.g., using ELNs) is critical for ensuring future data complies with **FAIR** principles.

## Future Directions

The project will focus on ensuring that community data inputs adhere to **FAIR** standards and that researchers and students are educated in these practices from the outset. Publications and funders will increasingly demand **FAIR**-compliant datasets. The **Data-Access Visualizer** and **EPMA Data Tools** will continue to evolve with input from the community, with opportunities for collaboration and development through hackathons and other low-cost events.
